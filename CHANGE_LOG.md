## Change Log

### v0.1.0

* add scripts to test the module
* create LICENSE.md file
* create CHANGE_LOG.md file
* add [NodeICO](https://nodei.co/) badge

### v0.0.1

* first release