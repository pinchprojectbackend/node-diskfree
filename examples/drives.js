/* require the module */
var df = require('../lib/index.js');

/* retrieve disks list */
df.drives(function (err, drives) {
    if (err) return console.log(err);

    /* retrieve space information for each drives */
    df.drivesDetail(drives, function (err, data) {
        if (err) return console.log(err);
        console.log(data);
    });
});