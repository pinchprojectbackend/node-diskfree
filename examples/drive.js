/* require the module */
var df = require('../lib/index.js');

/* retrieve disks list */
df.drives(function (err, drives) {
    if (err) return console.log(err);

    /* retrieve space information for one drive */
    df.driveDetail(drives[0], function (err, data) {
        if (err) return console.log(err);
        console.log(data);
    });
});