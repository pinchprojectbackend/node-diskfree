# node-diskfree - [![NPM version](https://badge.fury.io/js/node-diskfree.png)](http://badge.fury.io/js/node-diskfree)

[![NPM](https://nodei.co/npm/node-diskfree.png?downloads=true&stars=true)](https://nodei.co/npm/node-diskfree/)

Get used, available and total disk space. This module using 'df -k' for OSX system and 'df' for *nix system.

## Installation

```
npm install node-diskfree
```

## Usage

```js
var df = require('node-diskfree');

/* retrieve disks list */
df.drives(
	function (err, drives) {
		if (err) {
			return console.log(err);
		}
		
		/* retrieve space information for each drives */
		df.drivesDetail(
			drives,
			function (err, data) {
				if (err) {
					return console.log(err);
				}
				
				console.log(data);
			}
		);
		
		/* or retrieve space information for on drive */
		df.driveDetail(
			drives[0],
			function (err, data) {
				if (err) {
					return console.log(err);
				}
				
				console.log(data);
			}
		);
	}
);
```

## Tests

You can run these scripts :

* to get information about first drive

```
npm run-script drive
```

* to get information about all drives

```
npm run-script drives
```

## License

See LICENSE.md file.

## Change Log

See CHANGE_LOG.md file.